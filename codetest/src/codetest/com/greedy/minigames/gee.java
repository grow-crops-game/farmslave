package codetest.com.greedy.minigames;

import java.util.Scanner;

public class gee {
	
	static public int gameNum = 0;
	int player=0; //플레이어 패
	int com=0; //컴퓨터 패
	int i=0;//while 조건 걸어서 반복 막아봤어요
	Scanner sc = new Scanner(System.in);

	// 프로그램 시작
	
	public void geeMethod() {
	while(i<1) {
		com = (int)((Math.random()*3)+1); //컴퓨터 패 선택
		
		//플레이어 패 입력 시작
		while(i<1) {
			try {//플레이어 패 입력
				System.out.println("\n=========쥐 잡기 게임=========");
				System.out.println("ʕ•̭͡•ʕ•̯ͦ͡•ʕ•̻̀•́ʔ 부스럭..쮝찍찍 찍찍!!");
				System.out.println("?!???!!!!으악!!!  지...쥐?!쥐야???");
				System.out.println("안돼!! 내 농작물 먹고 있어");
				System.out.println("쥐로부터 농작물을 지키기 위해 방법을 찾아야해");
				System.out.println("");
				System.out.println("당신의 선택은???");
				System.out.print("숫자를 선택하세요 (1.안심 세스코 / 2.고전방법 쥐덫 / 3.설탕바른 쥐약) : ");
				player=Integer.parseInt(sc.nextLine()); //플레이어 패 입력
				if(player==1 || player==2 || player==3) { //선택지의 입력이 들어오면
					//컴퓨터의 패 표기
					if(com==1) {
						System.out.println(" 어떤 방법이 가장 좋을까?");
					}else if(com==2) {
						System.out.println("쥐들이 다 사라져야 될텐데...");
					}else if(com==3) {
						System.out.println("빨리 해결하지 않으면 안되겟어");
					}

					//플레이어의 패 표기
					if(player==1) {
						System.out.println("역시 안심 세스코가 답이겠지!");
					}else if(player==2) {
						System.out.println("저렴하고 고전적인 쥐덫 방법이면 되겠지!");
					}else if(player==3) {
						System.out.println("농작물 대신 설탕바른 쥐약을 먹게하면 해결될거야!");
					}

					//승패판정
					if((player==1&&com==3)||(player==2&&com==1) ||(player==3&&com==2)) {
						System.out.println("역시 내 방법이 옳았어!!쥐가 사라지고 있어!\\(❁´∀`❁)ﾉ");
						gameNum = 1;
					}else if((player==3&&com==1)||(player==1&&com==2) ||(player==2&&com==3)) {
						System.out.println("뭐야ㅠ 이게 아닌가..? 쥐들이 하나도 안줄었자나...(ಥ﹏ಥ)");
					}else if(player==com){
						System.out.println("줄긴 한 것 같긴한데 여전히 많네.....(๑´╹‸╹`๑)");
					}
					++i;

					break;
				}else {//선택지 외 숫자 입력
					System.out.println("선택지의 숫자를 입력해주세요.");
					continue; 
				}

			}catch(Exception e) {//문자입력시 예외처리
				System.out.println("숫자를 입력해주세요.");
			}
		}          
		
	}
	}

	public static int getGameNum() {
		return gameNum;
	}

	public static void setGameNum(int gameNum) {
		gee.gameNum = gameNum;
	}
	
	
	
	
	
	
}
