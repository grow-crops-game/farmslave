package codetest.com.greedy.minigames;

import java.util.Scanner;

public class RockPaperScissors {

	/**
	 * 보너스 미니게임 2. 가위바위보
	 * 1~3까지의 랜덤한 정수를 가위 바위 보에 대입한다.
	 * 대입한 가위 바위 보에서 승리할 경우, 추가 포인트와 골드 획득!
	 * 패배할 경우 실패하게 된다.		
	 * 
	 */
	static public int gameNum = 0;
	
	public void RockPaperScissorsMethod() {
	Scanner sc = new Scanner(System.in);

		int num = (int)(Math.random() * 3)+1;
		System.out.println("야생의 멧돼지가 나타나 가위바위보를 신청했다!");
		System.out.println("농작물들을 빼앗기지 않으려면 가위바위보에서 이겨야 한다!");
		System.out.println("가위바위보를 입력하세요.");
		System.out.println("1. 가위 / 2. 바위 / 3. 보");
		int chance = sc.nextInt();
		
		
		while(true) {
			int result = chance - num;
			switch(result) {
			case 0 :
				System.out.println("비겼습니다. 다시 한 번 더 !");
				System.out.println("가위바위보를 입력하세요.");
				System.out.println("1. 가위 / 2. 바위 / 3. 보");
				chance = sc.nextInt();
				num = (int)(Math.random() * 3)+1;
				continue;
			case 1 : case -2 :
				System.out.println("이겼습니다!");
				System.out.println("멧돼지는 다음을 노리며 퇴장했다!");
				gameNum = 1;
				break;
			case -1 : case 2 :
				System.out.println("졌습니다!");
				System.out.println("멧돼지는 농작물들을 먹어치우고 사라졌다.");
				System.out.println("기껏 잘 지어놨는데.....! 두고보자!");
				break;
		}
				if(result != 0) {
					break;
				}
		}
		
	}

	public static int getGameNum() {
		return gameNum;
	}

	public static void setGameNum(int gameNum) {
		RockPaperScissors.gameNum = gameNum;
	}
	
	
	
}