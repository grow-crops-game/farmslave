package codetest.com.greedy.GTest;

import java.util.Scanner;

import codetest.com.greedy.vgInfo.Carrot;
import codetest.com.greedy.vgInfo.Potato;
import codetest.com.greedy.vgInfo.Tomato;
import codetest.com.greedy.vgInfo.Vegetable;

public class Application1 {

	public static void main(String[] args) {
		System.out.println("시작");

		/**
		 * [개발새발 팀 : 다형성 게임 작성 과제] 프로젝트명 : farmslave 게임 설명 : 부푼 꿈을 안고 귀농을 한 주인공이 3가지 작물
		 * 중 하나를 선택해 키우는 게임. 작물을 성장시킬 수 있는 기회는 10회(임의). 회차마다 [물주기][퇴비주기][농약뿌리기] 3가지 선택지
		 * 중 하나를 골라 확률적으로 성공할 때마다 수확 point(pt)를 획득한다. 10회의 기회를 모두 소모하였을 때, 작물이
		 * 1200pt(임의) 만큼 성장하였을 경우 농사 성공! 실패할 경우, 주인공은 좌절의 마음을 안고 다시 서울로 상경하거나, 다시 농사를
		 * 지어볼 수 있다.
		 * 
		 * 2022.07.07~2022.07.11 개발새발 팀 : 김승환 / 김형기 / 유성준 / 안세용 / 안나영 / 정혜연
		 */
		
		
		
		int count = 1;
		int myGold = 700;
		int goalPoint = 1000;
		int sumPoint = 0;
		int endset = 0;

		String seed = null;
		Vegetable veget = new Vegetable();
		Tomato tomato = new Tomato();
		Carrot carrot = new Carrot();
		Potato potato = new Potato();

		Scanner sc = new Scanner(System.in);

		System.out.println("******************** Stop do BBalri ********************");
		System.out.println("게임 규칙");
		System.out.println("@ 초기 자본 700이 주워집니다! 해당 자본으로 작물을 재배 할수 있습니다!");
		System.out.println("@ 당근: 90pt, 감자 : 40pt, 토마토 :120pt 부여됩니다!");
		System.out.println("@ 작물을 심을 때는 자본을 이용하여 해당 턴에 특수한 효과를 부여할수 있습니다!");
		System.out.println("@ 작물을 재배하여 점수와 자본을 획득! 목표 점수까지 도달하면 게임 클리어 입니다!");
		System.out.println("@ 턴 수가 10번이 넘어가거나 자본이 -가 되면 게임 오버 입니다!");
		System.out.println("@작물 재배에 성공시 랜덤한 확률로 미니겜이 시작됩니다! 미니게임에서 승리하여 추가 점수와 골드를 획득하세요!");
		System.out.println();
		System.out.println("============GAME START!!!===========");
		
		
	
		System.out.println("요즘 대세는 귀농이라지!");
		System.out.println("고된 서울 살이에 지친 우별림은 귀농의 꿈을 꾸며 부푼 마음으로 시골로 향했다.");
		System.out.println("하지만 성공적인 귀농 살이를 위해서는 성공적인 농사를 지어야 하는 법....");
		System.out.println("과연 우별림은 행복한 귀농 생활을 이어갈 수 있을까?");
		System.out.println("------------------------------");
		System.out.println();
		
		while (sumPoint < goalPoint) {
			System.out.println("현재 " + count + "번째 턴입니다!");
			System.out.print("재배할 작물을 선택하세요!");
			System.out.println();
			System.out.print("1. 당근  / 2. 감자  / 3. 토마토(50골드 소모) : ");
			int chooseVegetable = sc.nextInt();

			switch (chooseVegetable) {
			case 1:
				seed = "당근";
				System.out.println(seed + "을(를) 선택 하셨습니다!");
				carrot.ptc();
				carrot.seed();
				carrot.success();
				break;
			case 2:
				seed = "감자";
				System.out.println(seed + "을(를) 선택 하셨습니다!");
				potato.ptc();
				potato.seed();
				potato.success();
				break;
			case 3:
				seed = "토마토";
				System.out.println(seed + "을(를) 선택 하셨습니다!");
				tomato.ptc();
				tomato.seed();
				tomato.success();
				break;

			}

			int minusGold = veget.getUsedGold();
			int plusGold = veget.getMoney();
			int point = veget.getPoint();

			sumPoint = sumPoint + point;
			myGold = myGold + plusGold - minusGold;

			count++;

			System.out.println();


			System.out.println("현재 점수 : " + sumPoint + ", 현재 보유 자금 : " + myGold);
			System.out.println();
			if (count == 10) {
				System.out.println("횟수가 초과 되었습니다!");
				endset += 10;
			}

			if (myGold <= 0) {
				System.out.println("당신은 파산했습니다! 사용한 턴 수 : " + count);
				endset += 10;
		
			}

			if (endset > 0 ) {
				System.out.println();
				System.out.println("게임을 이어서 하시겠습니까?");
				System.out.println("Y or N");
				char input = sc.next().charAt(0);
					if(input == 'N') {
						System.out.println("Game Over");
						break;
					}
					if(input == 'Y') {
						count = 1;
						myGold = 700;
						goalPoint = 1000;
						sumPoint = 0;
						endset = 0;
					}else {
						System.out.println("오류 게임을 종료합니다.");
						break;
					}
					
			}
			
		}
		
		if(endset < 1 ) {
			System.out.println();
			System.out.println("게임 클리어!");
			System.out.println("사용 턴 수 : " + count);
			System.out.println("최종 점수 : " + sumPoint);
			System.out.println("최종 보유 자금 : " + myGold);
			System.out.println("종합 점수 : " + (sumPoint + (myGold / 20) - (count * 10)));
		}
		
		
		

		
		
		
		
		
		
		
	}
}

