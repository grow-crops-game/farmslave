package codetest.com.greedy.vgInfo;

public class Carrot extends Vegetable{
	
	public Carrot() {}
	
	@Override
	public void ptc() {
		
				super.ptc();
				System.out.println("활동을 선택해주세요!");
				System.out.println("[아이템 효능] \n1.농약치기 : 500골드 소모, 재배 실패 확률을 무효화합니다!");//아이템 효능 알려주기 멘트 추가했어요
				System.out.println("2.비료주기 : 150골드 소모, 재배 성공 시 수확점수가 2배가 됩니다!");//
				System.out.println("3.물주기	: 40골드 소모, 재배 성공시 골드를 추가 획득합니다!");//멘트수정
				System.out.print("1. 농약치기 2. 비료주기 3. 물주기 : ");
				int ptcq = sc.nextInt();
				bonus = 0;
				point = 90;
				money = 30;
				
				switch (ptcq){
					case 1 : ranNum = (int)(Math.random()*100+101);
					System.out.println("500골드를 소모하여 당근에 농약을 사용! 재배 실패 확률을 무효화합니다!");
					usedGold = 500;
					Fertilizer = 0;
					water = 0;
					
					
					break;
					case 2 : point += point;
					System.out.println("150골드를 소모하여 당근에 비료를 사용! 재배 성공시 수확 점수가 2배가 됩니다!");
					usedGold = 150;
					Fertilizer = 1;
					water = 0;
					break;
					case 3 : 
						System.out.println("40골드를 소모하여 당근에 물을 줍니다! 보너스 게임 승리시 골드획득량이 증가합니다!");
						usedGold = 40;
						gameMoney += money;
						water = 1;
						Fertilizer = 0;
						break;
					}
					
				}
	@Override
	public void seed() {
		System.out.println("당근을 심었습니다! 재배를 시작할게요!");
		
	}
	
	@Override
	public void success() {

		if(ranNum <= 60) {
			bonus = 0;
			point = 0;
			switch(failNum) {
			case 0 : System.out.println("야생의 고라니가 당근을 먹어버렸다!");
			break;
			case 1 : System.out.println("홍수로 인해 당근이 침수당했다!");
			break;
			case 2 : System.out.println("벌레들이 당근을 갉아 먹어버렸다!");
			break;
			}
			
		} else if(ranNum >= 140) {
			System.out.println("재배를 성공하였습니다.");
			System.out.println("보너스 게임 발동!!");
			int gameNum = (int)(Math.random()*3);
			switch(gameNum) {
			case 0 : System.out.println("콩게임");
			kd.kongUpAndDownMethod();
			int kongBonus = kd.getGameNum();
			if(kongBonus > 0) {
				bonus += point;
				if( Fertilizer > 0) {
				point += (bonus/2);
				}else {
					point += bonus;
				}
				System.out.println();
				System.out.println("게임에서 승리 하셨습니다! 보너스 점수가 " + bonus + "로 상승!!!");
				if(water > 0) {
					gameMoney += money;
					money += gameMoney;
					System.out.println("물주기의 효과로 추가 골드를 획득합니다!" + money +"획득!!!");
					System.out.println();
					System.out.println("당근 재배에 성공합니다. " + point + "점과 " + money + "골드 획득!");
				}
				
			}
			break;
			case 1 : System.out.println("가위바위보");
			rps.RockPaperScissorsMethod();
			int RPSBonus = rps.getGameNum();
			if(RPSBonus > 0) {
				bonus += point;
				if( Fertilizer > 0) {
					point += (bonus/2);
					}else {
						point += bonus;
					}
				System.out.println("게임에서 승리 하셨습니다! 보너스 점수가 " + bonus + "로 상승!!!");
				if(water > 0) {
					gameMoney += money;
					money += gameMoney;
					System.out.println("물주기의 효과로 추가 골드를 획득합니다!" + money +"획득!!!");
					System.out.println();
					System.out.println("당근 재배에 성공합니다. " + point + "점과 " + money + "골드 획득!");
				}
			}
			break;
			case 2 : System.out.println("쥐잡기");	
			g.geeMethod();
			int gBonus = g.getGameNum();
			if(gBonus > 0) {
				bonus += point;
				if( Fertilizer > 0) {
					point += (bonus/2);
					}else {
						point += bonus;
					}
				System.out.println("게임에서 승리 하셨습니다! 보너스 점수가 " + bonus + "로 상승!!!");
				if(water > 0) {
					gameMoney += money;
					money += gameMoney;
					System.out.println("물주기의 효과로 추가 골드를 획득합니다!" + money +"획득!!!");
					System.out.println();
					System.out.println("당근 재배에 성공합니다. " + point + "점과 " + money + "골드 획득!");
				}
			}
			break;
			
			}
			
			
			
		}
		else { 
			System.out.println();
			point += bonus;
			System.out.println("당근 재배에 성공합니다. " + point + "점과 " + money + "골드 획득!");
			
			
		} 
	}
		
}
