package codetest.com.greedy.vgInfo;

import java.util.Scanner;

import codetest.com.greedy.minigames.RockPaperScissors;
import codetest.com.greedy.minigames.gee;
import codetest.com.greedy.minigames.kongUpAndDown;

public class Vegetable {

	//game method 호출
	kongUpAndDown kd = new kongUpAndDown();
	RockPaperScissors rps = new RockPaperScissors();
	gee g = new gee();
	//test texture
	int ranNum = 0; // 작물 재배 성공 실패용 변수
	int failNum = 0; //실패시 랜덤 대사용 변수

	static public int point = 0; // 획득 점수
	static public int usedGold = 0; // 소모자금
	static public int money = 0; //획득한 돈
	int bonus = 0;
	int gameMoney = 0;
	int water = 0;
	int Fertilizer = 0;
	
	Scanner sc = new Scanner(System.in);
	
	public Vegetable() {}
	
	//[농약뿌리기]
	public void ptc() {

		ranNum = (int)(Math.random()*200+1);
		failNum = (int)(Math.random()*3);
//		
//		System.out.println("활동을 선택해주세요!");
////		System.out.println("[아이템 효능] \n1.농약치기 : 500골드 소모, 재배 실패 확률을 무효화합니다!");//아이템 효능 알려주기 멘트 추가했어요
////		System.out.println("2.비료주기 : 150골드 소모, 재배 성공 시 수확점수가 2배가 됩니다!");//
////		System.out.println("3.물주기	: 40골드 소모, 재배 성공시 골드 획득량이 증가합니다!");//멘트수정
//		System.out.print("1. 농약치기 2. 비료주기 3. 물주기 : ");
//		int ptcq = sc.nextInt();
//		
//
//		switch (ptcq){
//			case 1 : ranNum = (int)(Math.random()*100+101);
//			System.out.println("500골드를 소모하여 농약을 사용! 재배 실패 확률을 무효화합니다!");
//			usedGold = 500;
//			
//			break;
//			case 2 : bonus = 100;
//			System.out.println("150골드를 소모하여 비료를 사용! 재배 성공시 수확 점수가 2배가 됩니다!");
//			usedGold = 150;
//			break;
//			case 3 : 
//				System.out.println("40골드를 소모하여 작물에 물을 줍니다! 재배 성공시 보너스 게임이 발생할 확률이 상승합니다!");
//				usedGold = 40;
//				break;
//			}
//			
		}
	

	//update test
	public void seed() {
		System.out.println("작물을 심었습니다! 재배를 시작할게요!");
		
	}
	
	
	public void success() {

		if(ranNum <= 100) {
			switch(failNum) {
			case 0 : System.out.println("야생의 고라니가 작물을 먹어버렸다!");
			break;
			case 1 : System.out.println("홍수로 인해 작물이 침수당했다!");
			break;
			case 2 : System.out.println("벌레들이 작물을 갉아 먹어버렸다!");
			break;
			}
			
		}else {
			System.out.println();
			point = 100 + bonus; // 각 작물별로 기본 할당 점수값 변경 필요
			money = 30;			 // 각 작물별로 기본 할당 골드량 변경 필요
			System.out.println("재배에 성공합니다. " + point + "점과 " + money + "골드 획득!");
		}
	}
		
	public Vegetable(int ranNum, int point, int usedGold, int money, int failNum, Scanner sc) {
		super();
		this.ranNum = ranNum;
		this.point = point;
		this.usedGold = usedGold;
		this.money = money;
		this.failNum = failNum;
		this.sc = sc;
	}

	
	
	
	public int getWater() {
		return water;
	}

	public void setWater(int water) {
		this.water = water;
	}

	public int getGameMoney() {
		return gameMoney;
	}

	public void setGameMoney(int gameMoney) {
		this.gameMoney = gameMoney;
	}

	public int getRanNum() {
		return ranNum;
	}

	public void setRanNum(int ranNum) {
		this.ranNum = ranNum;
	}

	public int getPoint() {
		return point;
	}

	public void setPoint(int point) {
		this.point = point;
	}

	public int getUsedGold() {
		return usedGold;
	}

	public void setUsedGold(int usedGold) {
		this.usedGold = usedGold;
	}

	public int getMoney() {
		return money;
	}

	public void setMoney(int money) {
		this.money = money;
	}

	public int getFailNum() {
		return failNum;
	}

	public void setFailNum(int failNum) {
		this.failNum = failNum;
	}

	public Scanner getSc() {
		return sc;
	}

	public void setSc(Scanner sc) {
		this.sc = sc;
	}

	public int getFertilizer() {
		return Fertilizer;
	}

	public void setFertilizer(int fertilizer) {
		Fertilizer = fertilizer;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//check please
	
	
//	char ptcqUpperCase = Character.toUpperCase(ptcq);
//	if(ptcqUpperCase == 'Y')

}


